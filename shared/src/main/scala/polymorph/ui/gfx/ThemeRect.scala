package polymorph.ui.gfx

import polymorph.geom.Insets

case class ThemeRect(image: UIImage, insets: Insets)
