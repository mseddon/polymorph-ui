package polymorph.ui.gfx

import java.nio.{ByteBuffer, ByteOrder}

import polymorph.Resource
import polymorph.gfx.{GlobalCompositeOperation, Image, Color}

import scala.annotation.tailrec
import scala.concurrent.Future

/**
 * A raster font that renders from a texture atlas.  This is the fastest way to blit text onto the screen under opengl,
 * which is the primary renderpath for polymorph-ui.
 *
 * NOTE: you will experience slow performance under the 2d renderpath for coloured text.  If you require blitting more
 * than a few characters to the screen in a particular colour, you should request an atlas is generated specifically for
 * this colour using `optimizeColor`.  This is a no-op under the OpenGL renderpath, and consumes memory under the 2d
 * renderpath.  It effectively colorizes the font image for that particular colour, making subsequent text rendering
 * in that colour *way* faster.
 *
 * The drawbacks are that currently the characters must all fit on
 * a single texture upfront (so CJK is out), and we do not perform glyph shaping, so really we stick to basic latin,
 * cyrillic and greek style languages for now.  Fonts are generated with the fgen tool.  Some work towards overcoming
 * these shortcomings are currently in the `scriptorium` project, which aims to someday be a fairly complete OpenType
 * reader and glyph shaper, but this is a *lot* of work, and my primary targets are the US and Europe, so progress is
 * slow here.  Contributions welcome!
 *
 * @param height the height of this font
 * @param ascent the ascent for each character in the font
 * @param charMap map unicode codepoint onto a glyph id
 * @param glyphs the glyph rendering information.
 * @param kern the kerning pair table.
 * @param parent the font that is
 * @param baseColor the colour this font is generated from/
 * @param image the base image for this font atlas
 */
class UIFont(
  val height: Int,
  val ascent: Int,
  val charMap: Map[Short,Short],
  val glyphs: Map[Int, UIFont.Glyph],
  val kern: Map[(Int, Int), Float],
  val parent: UIFont,
  val baseColor: Color,
  val image: UIImage) {
  val descent = height - ascent
  private var map = if(parent == null) Map[Color, UIFont]() else null

  @tailrec
  private def rootFont(): UIFont =
    if(parent == null) this
    else parent.rootFont()

  private[polymorph] def getOptimizedColor(color: Color): UIFont = {
    val root = rootFont()
    val base = color.copy(a = 1)
    root.map.getOrElse(base, null)
  }

  /**
   * Optimize this font for rendering in Color. In the GL renderpath this does nothing,
   * but for Canvas implementations (which don't support multiply on blit), this creates
   * a colourized version of this font's image to use for that colour.  UI themes should always
   * invoke this where necessary, and avoid allocating too many colours...
   *
   * Simply by invoking this method on a colour you wish to render in will result in several orders
   * of magnitude performance gains, at the (not insignificant) cost of additional memory.
   *
   * @param color the colour to render in.
   */
  def optimizeColor(color: Color): UIFont = {
    val out = getOptimizedColor(color)
    if(out != null)
      out
    else image match {
      case img: UIGfxImage =>
        val img2 = Image(img.width, img.height)
        val gfx = img2.gfx
        val base = color.copy(a = 1)
        gfx.style = base
        gfx.fillRect(0, 0, img.width, img.height)
        gfx.globalCompositeOperation = GlobalCompositeOperation.DstIn
        gfx.drawImage(img.image, 0, 0)
        val root = rootFont()
        val font = new UIFont(height, ascent, charMap, glyphs, kern, root, color, new UIGfxImage(img2))
        root.map += (base -> font)
        font
      case _ =>
        this
    }
  }

  /**
   * Returns the width of a string of text rendererd in this font.
   * @param string the string to measure.
   */
  def stringWidth(string: String): Float = {
    if(string.isEmpty)
      return 0
    var width = 0f
    var prev = 0
    for(i <- string.indices) {
      val glyph = glyphs(charMap(string.charAt(i).toShort))
      width += kern.get(prev, glyph.id).getOrElse(0f) + glyph.advanceX
      prev = glyph.id
    }
    width
  }
}

object UIFont {
  case class Glyph(
    id: Int,
    atlasX: Int,
    atlasY: Int,
    width: Int,
    height: Int,
    offsetX: Int,
    offsetY: Int,
    advanceX: Int,
    advanceY: Int
  )

  import concurrent.ExecutionContext.Implicits.global

  def fromResource(name: String)(implicit uiContext: UIContext): Future[UIFont] =
    Resource.getResource(name).flatMap(buf => fromByteBuffer(buf))

  def fromByteBuffer(buffer: ByteBuffer)(implicit uIContext: UIContext): Future[UIFont] = {
    buffer.order(ByteOrder.BIG_ENDIAN)
    val height = buffer.getShort
    val ascent = buffer.getShort

    val charMap: Map[Short, Short] = {
      for (i <- 0 until buffer.getShort & 0xFFFF)
        yield (buffer.getShort, buffer.getShort)
    }.toMap

    val glyphs = {
      val count = buffer.getShort
      for (i <- 0 until count)
        yield {
          val id = buffer.getShort.toInt
          (id, Glyph(
            id,
            buffer.getShort,
            buffer.getShort,
            buffer.getShort,
            buffer.getShort,
            buffer.getShort,
            buffer.getShort,
            buffer.getShort,
            buffer.getShort
          ))
        }
    }.toMap[Int, Glyph]

    val scale = buffer.getFloat
    val kern = (for(i <- 0 until buffer.getShort & 0xFFFF)
      yield (buffer.getShort & 0xFFFF, buffer.getShort & 0xFFFF) -> buffer.getShort*scale).toMap

    UIImage.fromByteBuffer(buffer).map { image =>
      new UIFont(height, ascent, charMap, glyphs, kern, null, Color(1,1,1), image)
    }
  }
}