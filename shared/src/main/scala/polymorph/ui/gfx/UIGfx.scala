package polymorph.ui.gfx

import java.nio.ByteBuffer

import polymorph.geom.Rect
import polymorph.gfx._
import polymorph.gl.GL
import polymorph.ui.RootGLView
import polymorph.ui.gfx.gl.{ImageBlitter, ImageCache, SubImage}
import scryetek.vecmath.{Mat2d, Vec2}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Promise, Future}
import scala.language.implicitConversions
trait UIContext {
  private[polymorph] def uiImageFromByteBuffer(byteBuffer: ByteBuffer): Future[UIImage]
  private[polymorph] def uiImageFromResource(file: String): Future[UIImage]
  private[polymorph] def uiImageFromImage(image: UIImage): UIImage
  //private[polymorph] def uiFontFromResource(file: String): Future[UIImage]
}


trait UIImage {
  def width: Int
  def height: Int
}


object UIImage {
  def fromImage(image: UIImage)(implicit context: UIContext): UIImage =
    context.uiImageFromImage(image)

  def fromResource(file: String)(implicit context: UIContext): Future[UIImage] =
    context.uiImageFromResource(file)

  def fromByteBuffer(byteBuffer: ByteBuffer)(implicit context: UIContext): Future[UIImage] =
    context.uiImageFromByteBuffer(byteBuffer)
}

object UIGfx {
  val white = Color(1,1,1)
}

trait UIGfx {
  def translate(t: Vec2): Unit
  def rotate(angle: Float): Unit
  def scale(s: Vec2): Unit
  def save(): Unit
  def restore(): Unit

  def clear(colour: Color): Unit

  def drawImage(img: UIImage, dx: Float, dy: Float): Unit

  def drawImage(img: UIImage, dx: Float, dy: Float, dwidth: Float, dheight: Float): Unit

  def drawImage(img: UIImage, sx: Float, sy: Float, swidth: Float, sheight: Float,
    dx: Float, dy: Float, dwidth: Float, dheight: Float): Unit

  private[polymorph] def drawImage(img: UIImage, sx: Float, sy: Float, swidth: Float, sheight: Float,
    dx: Float, dy: Float, dwidth: Float, dheight: Float, color: Color): Unit

  def drawString(x: Int, y: Int, string: String, color: Color, font: UIFont): Unit = {
    if(string.isEmpty)
      return
    var _font = font.getOptimizedColor(color)
    val _color = if(_font != null) UIGfx.white else color
    if(_font == null) _font = font
    var sx = x.toFloat// - font.glyphs(font.charMap(string.head.toShort)).offsetX
    var sy = y.toFloat - font.descent
    var prev = -1
    for(i <- string.indices) {
      val glyph = font.glyphs(font.charMap(string.charAt(i).toShort))
      sx += font.kern.get(prev, glyph.id).getOrElse(0f)
      drawImage(
        _font.image,
        glyph.atlasX, glyph.atlasY, glyph.width, glyph.height,
        (glyph.offsetX + sx).toInt, (glyph.offsetY+sy).toInt, glyph.width, glyph.height, _color)

      prev = glyph.id
      sx += glyph.advanceX
      sy += glyph.advanceY
    }
  }

  def drawThemeRect(themeRect: ThemeRect, x: Float, y: Float, width: Float, height: Float): Unit = {
    import themeRect.{insets, image}

    // top row
    drawImage(image, 0, 0, insets.left, insets.top, x, y, insets.left, insets.top)
    drawImage(image, insets.left, 0, image.width-insets.width, insets.top, x+insets.left, y, width-insets.width, insets.top)
    drawImage(image, image.width-insets.right, 0, insets.right, insets.top, x+width-insets.right, y, insets.right, insets.top)

    // middle row
    drawImage(image, 0, insets.top, insets.left, image.height-insets.height, x, y+insets.top, insets.left, height-insets.height)
    drawImage(image, insets.left, insets.top, image.width-insets.width, image.height-insets.height, x+insets.left, y+insets.top, width-insets.width, height-insets.height)
    drawImage(image, image.width-insets.right, insets.top, insets.right, image.height-insets.height, x + width - insets.right, y+insets.top, insets.right, height-insets.height)

    // bottom row
    drawImage(image, 0, image.height-insets.bottom, insets.left, insets.bottom, x, y+height-insets.bottom, insets.left, insets.bottom)
    drawImage(image, insets.left, image.height-insets.bottom, image.width-insets.width, insets.bottom, x+insets.left, y+height-insets.bottom, width-insets.width, insets.bottom)
    drawImage(image, image.width-insets.right, image.height-insets.bottom, insets.right, insets.bottom, x+width-insets.right, y+height-insets.bottom, insets.right, insets.bottom)
  }

  def drawThemeRect(themeRect: ThemeRect, rect: Rect): Unit =
    drawThemeRect(themeRect, rect.x, rect.y, rect.width, rect.height)

  def clipRect(x: Float, y: Float, width: Float, height: Float): Unit

  def fillRect(x: Float, y: Float, width: Float, height: Float, color: Color): Unit
}

///////////////////////// GL Implementations

class UIGLContext(val view: RootGLView, var gl: GL, val imageCache: ImageCache, val imageBlitter: ImageBlitter) extends UIContext {
  val white = {
    val whiteImage = Image(3,3)
    val g = whiteImage.gfx
    g.style = Color(1,1,1)
    g.fillRect(0, 0, 3, 3)
    imageCache.add(whiteImage)
  }

  def uiImageFromByteBuffer(buffer: ByteBuffer): Future[UIImage] =
    Image.fromByteBuffer(buffer).flatMap { image =>
      val promise = Promise[UIImage]()
      view.withGL { gl =>
        this.gl = gl
        promise.success(new UIGLImage(imageCache.add(image)))
      }
      promise.future
    }

  def uiImageFromResource(file: String): Future[UIImage] =
    Image.fromResource(file).flatMap { image =>
      val promise = Promise[UIImage]()
      view.withGL { gl =>
        this.gl = gl
        promise.success(new UIGLImage(imageCache.add(image)))
      }
      promise.future
    }

  def uiImageFromImage(image: UIImage): UIImage = {
    val img = imageCache.addPacked(image.asInstanceOf[UIGLImage].subImage.image)
    new UIGLImage(img)
  }
}


class UIGLImage(val subImage: SubImage) extends UIImage {
  def width = subImage.width
  def height = subImage.height
}

class UIGLGfx(context: UIGLContext, width: Int, height: Int) extends UIGfx {
  implicit def toImage(uiImage: UIImage): SubImage =
    uiImage.asInstanceOf[UIGLImage].subImage

  def clear(colour: Color): Unit = {
    if(context.imageBlitter.noClipping) {
      context.gl.clearColor(colour.r, colour.g, colour.b, colour.a)
      context.gl.clear(GL.COLOR_BUFFER_BIT)
      context.imageBlitter.reset()
    } else {
      val transform = Mat2d(context.imageBlitter.transform)
      context.imageBlitter.transform.setIdentity()
      context.imageBlitter.drawImage(context.white, 1, 1, 0, 0, 0, 0, width, height, colour)
      context.imageBlitter.transform.set(transform)
    }
  }

  def translate(t: Vec2): Unit =
    context.imageBlitter.translate(t)

  def rotate(angle: Float): Unit =
    context.imageBlitter.rotate(angle)

  def scale(s: Vec2): Unit =
    context.imageBlitter.scale(s)

  def save(): Unit =
    context.imageBlitter.save()

  def restore(): Unit =
    context.imageBlitter.restore()


  def drawImage(img: UIImage, dx: Float, dy: Float): Unit =
    context.imageBlitter.drawImage(img, dx, dy)

  def drawImage(img: UIImage, dx: Float, dy: Float, dwidth: Float, dheight: Float): Unit =
    context.imageBlitter.drawImage(img, dx, dy, dwidth, dheight)

  def drawImage(img: UIImage, sx: Float, sy: Float, swidth: Float, sheight: Float,
    dx: Float, dy: Float, dwidth: Float, dheight: Float): Unit =
    context.imageBlitter.drawImage(img, sx, sy, swidth, sheight, dx, dy, dwidth, dheight)

  private[polymorph]
  def drawImage(img: UIImage, sx: Float, sy: Float, swidth: Float, sheight: Float, dx: Float, dy: Float, dwidth: Float, dheight: Float, color: Color): Unit =
    context.imageBlitter.drawImage(img, sx, sy, swidth, sheight, dx, dy, dwidth, dheight, color)

  def clipRect(x: Float, y: Float, width: Float, height: Float): Unit =
    context.imageBlitter.clipRect(x, y, width, height)

  def fillRect(x: Float, y: Float, width: Float, height: Float, color: Color): Unit =
    context.imageBlitter.drawImage(context.white, x, y, width, height, color)

}

//////////////////////// Gfx implementations

class UIGfxContext extends UIContext {
  val blendBuffer = Image(1024, 1024)
  val blendBufferGfx = blendBuffer.gfx

  def uiImageFromResource(file: String): Future[UIImage] =
    Image.fromResource(file).map(image => new UIGfxImage(image))

  def uiImageFromByteBuffer(byteBuffer: ByteBuffer): Future[UIImage] =
    Image.fromByteBuffer(byteBuffer).map(image => new UIGfxImage(image))

  def uiImageFromImage(image: UIImage): UIImage = {
    val img = Image(image.width, image.height)
    img.gfx.drawImage(image.asInstanceOf[UIGfxImage].image, 0, 0)
    new UIGfxImage(img)
  }
}

class UIGfxImage(val image: Image) extends UIImage {
  def width = image.width
  def height = image.height
}

class UIGfxGfx(context: UIGfxContext, gfx: Gfx, w: Int, h: Int) extends UIGfx {
  import UIGfx._
  implicit def toImage(uiImage: UIImage): Image =
    uiImage.asInstanceOf[UIGfxImage].image

  def clear(colour: Color): Unit = {
    gfx.style = colour
    gfx.fillRect(0, 0, w, h)
  }

  def drawImage(img: UIImage, dx: Float, dy: Float): Unit = {
    gfx.drawImage(img, dx, dy)
  }

  def drawImage(img: UIImage, dx: Float, dy: Float, dwidth: Float, dheight: Float): Unit = {
    if(dwidth == 0 || dheight == 0)
      return
    gfx.drawImage(img, dx, dy, dwidth, dheight)
  }

  def drawImage(img: UIImage, sx: Float, sy: Float, swidth: Float, sheight: Float,
    dx: Float, dy: Float, dwidth: Float, dheight: Float): Unit =
    if (swidth != 0 && sheight != 0 && dwidth != 0 && dheight != 0)
      gfx.drawImage(img, sx, sy, swidth, sheight, dx, dy, dwidth, dheight)

  def drawImage(img: UIImage, sx: Float, sy: Float, swidth: Float, sheight: Float,
    dx: Float, dy: Float, dwidth: Float, dheight: Float, color: Color): Unit = {
    if(swidth == 0 || sheight == 0 || dwidth == 0 || dheight == 0)
      return
    if(color == white)
      gfx.drawImage(img, sx, sy, swidth, sheight, dx, dy, dwidth, dheight)
    else {
      context.blendBufferGfx.globalCompositeOperation = GlobalCompositeOperation.SrcOver
      context.blendBufferGfx.style = color
      context.blendBufferGfx.fillRect(0, 0, dwidth, dheight)
      context.blendBufferGfx.globalCompositeOperation = GlobalCompositeOperation.DstIn
      context.blendBufferGfx.drawImage(img, sx, sy, swidth, sheight, 0, 0, dwidth, dheight)
      gfx.drawImage(context.blendBuffer, 0, 0, dwidth, dheight, dx, dy, dwidth, dheight)
    }
  }

  def clipRect(x: Float, y: Float, width: Float, height: Float): Unit = {
    val p = Path2d()
    p.moveTo(x, y)
    p.lineTo(x+width, y)
    p.lineTo(x+width, y+height)
    p.lineTo(x, y+height)
    p.close()
    gfx.clip(p)
  }

  override def translate(t: Vec2): Unit =
    gfx.translate(t)

  override def rotate(angle: Float): Unit =
    gfx.rotate(angle)

  override def save(): Unit =
    gfx.save()
  
  override def restore(): Unit =
    gfx.restore()

  override def scale(s: Vec2): Unit =
    gfx.scale(s)

  def fillRect(x: Float, y: Float, width: Float, height: Float, color: Color): Unit = {
    gfx.style = color
    gfx.fillRect(x, y, width, height)
  }
}