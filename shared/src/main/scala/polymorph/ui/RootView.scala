package polymorph.ui

import polymorph.Platform
import polymorph.gfx._
import polymorph.gl.GL
import polymorph.ui.gfx._
import polymorph.ui.gfx.gl._
import scryetek.vecmath.Mat4

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

import polymorph.event._

abstract class RootView(_title: String, _withId: String = null) extends AbstractRootView {
  private var _context: UIContext = null
  override def globalPosition = peerComponent.globalPosition

  val peerComponent = {
    try {
      if (!Platform.supportsOpenGL)
        throw new Throwable
      new RootGLView(_title, _withId) {
        private val eventTarget = RootView.this

        override def mouseExit(e: MouseExit) = eventTarget.mouseExit(e.copy(source = eventTarget))
        override def mouseEnter(e: MouseEnter) = eventTarget.mouseEnter(e.copy(source = eventTarget))
        override def mouseMove(e: MouseMove) = eventTarget.mouseMove(e.copy(source = eventTarget))
        override def mouseDrag(e: MouseDrag) = eventTarget.mouseDrag(e.copy(source = eventTarget))
        override def mouseDown(e: MouseDown) = eventTarget.mouseDown(e.copy(source = eventTarget))
        override def mouseUp(e: MouseUp) = eventTarget.mouseUp(e.copy(source = eventTarget))
        override def mouseWheel(e: MouseWheel) = eventTarget.mouseWheel(e.copy(source = eventTarget))

        override def keyDown(e: KeyDown) = eventTarget.keyDown(e.copy(source = eventTarget))
        override def keyUp(e: KeyUp) = eventTarget.keyUp(e.copy(source = eventTarget))
        override def keyTyped(e: KeyTyped) = eventTarget.keyTyped(e.copy(source = eventTarget))

        override def touchStart(e: TouchStart) = eventTarget.touchStart(e.copy(source = eventTarget))
        override def touchEnd(e: TouchEnd) = eventTarget.touchEnd(e.copy(source = eventTarget))
        override def touchMove(e: TouchMove) = eventTarget.touchMove(e.copy(source = eventTarget))
        override def touchCancel(e: TouchCancel) = eventTarget.touchCancel(e.copy(source = eventTarget))

        var initialized = false
        var firstInit = true
        val modelView = Mat4()
        override def paint(gl: GL): Unit = {
          if(initialized) {
            _context.asInstanceOf[UIGLContext].gl = gl
            if(firstInit) {
              _context.asInstanceOf[UIGLContext].imageCache.pack(gl)
              firstInit = false
            }
            _context.asInstanceOf[UIGLContext].imageBlitter.projectionMatrix.ortho(0, width, height, 0, 0, 100)
            _context.asInstanceOf[UIGLContext].gl = gl
            gl.enable(GL.BLEND)
            gl.blendFunc(GL.ONE, GL.ONE_MINUS_SRC_ALPHA)
            gl.stencilMask(0xff)
            gl.clearStencil(0)
            gl.clear(GL.STENCIL_BUFFER_BIT)
            gl.stencilMask(0)
            _context.asInstanceOf[UIGLContext].imageBlitter.reset()
            RootView.this.paint(new UIGLGfx(_context.asInstanceOf[UIGLContext], width.toInt, height.toInt))
            _context.asInstanceOf[UIGLContext].imageBlitter.draw(gl)
          }
        }

        override def init(gl: GL): Unit = {
          val cache = new ImageCache(gl, 2048, 2048)
          _context = new UIGLContext(
            this,
            gl,
            cache,
            new ImageBlitter(gl))

          RootView.this.init(_context).foreach { x =>
            initialized = true
            repaint()
          }
        }
      }
    } catch {
      case _: Throwable =>
        new RootGfxView(_title, _withId) {
          private val eventTarget = RootView.this

          override def mouseExit(e: MouseExit) = eventTarget.mouseExit(e.copy(source = eventTarget))
          override def mouseEnter(e: MouseEnter) = eventTarget.mouseEnter(e.copy(source = eventTarget))
          override def mouseMove(e: MouseMove) = eventTarget.mouseMove(e.copy(source = eventTarget))
          override def mouseDrag(e: MouseDrag) = eventTarget.mouseDrag(e.copy(source = eventTarget))
          override def mouseDown(e: MouseDown) = eventTarget.mouseDown(e.copy(source = eventTarget))
          override def mouseUp(e: MouseUp) = eventTarget.mouseUp(e.copy(source = eventTarget))
          override def mouseWheel(e: MouseWheel) = eventTarget.mouseWheel(e.copy(source = eventTarget))

          override def keyDown(e: KeyDown) = eventTarget.keyDown(e.copy(source = eventTarget))
          override def keyUp(e: KeyUp) = eventTarget.keyUp(e.copy(source = eventTarget))
          override def keyTyped(e: KeyTyped) = eventTarget.keyTyped(e.copy(source = eventTarget))

          override def touchStart(e: TouchStart) = eventTarget.touchStart(e.copy(source = eventTarget))
          override def touchEnd(e: TouchEnd) = eventTarget.touchEnd(e.copy(source = eventTarget))
          override def touchMove(e: TouchMove) = eventTarget.touchMove(e.copy(source = eventTarget))
          override def touchCancel(e: TouchCancel) = eventTarget.touchCancel(e.copy(source = eventTarget))

          var initialized = false

          override def paint(gfx: Gfx): Unit =
            if (initialized)
              RootView.this.paint(new UIGfxGfx(_context.asInstanceOf[UIGfxContext], gfx, width.toInt, height.toInt))

          import scala.concurrent.ExecutionContext.Implicits.global

          _context = new UIGfxContext()
          RootView.this.init(_context).foreach { x =>
            initialized = true
            repaint()
          }
        }
    }
  }
  val peer = peerComponent.peer

  def paint(gfx: UIGfx): Unit
  def init(implicit context: UIContext): Future[Unit]

  override def cursor =
    peerComponent.cursor

  override def cursor_=(cursor: Cursor): Unit =
    peerComponent.cursor = cursor
}
