package polymorph.geom

/**
 * Created by matt on 06/10/15.
 */
object Anchor {
  case object Start extends Anchor
  case object End extends Anchor
  case object Both extends Anchor
  case object None extends Anchor
}

sealed trait Anchor