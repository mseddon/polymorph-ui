package polymorph.geom

import scryetek.vecmath.Vec2

final case class Rect(x: Float, y: Float, width: Float, height: Float) {
  def this(position: Vec2, size: Dimension) =
    this(position.x, position.y, size.width, size.height)

  def position = Vec2(x, y)

  def size = Dimension(width, height)

  def contains(v: Vec2): Boolean =
    v.x >= x && v.y >= y && v.x < x + width && v.y < y + height

  def +(v: Vec2) =
    copy(x = x + v.x, y = y + v.y)

  def -(v: Vec2) =
    copy(x = x - v.x, y = y - v.y)

  def +(v: Dimension) =
    copy(width = width + v.width, height = height + v.height)

  def -(v: Dimension) =
    copy(width = width - v.width, height = height - v.height)

  def -(insets: Insets): Rect =
    Rect(x + insets.left, y + insets.top, width - insets.width, height - insets.height)

  def +(insets: Insets): Rect =
    Rect(x - insets.left, y - insets.top, width + insets.width, height + insets.height)
}

object Rect {
  def apply(position: Vec2, size: Dimension): Rect =
    new Rect(position, size)
}
