package polymorph.geom

sealed trait Axis {
  val other: Axis
}

object Axis {
  case object Horizontal extends Axis {
    val other = Vertical
  }
  case object Vertical extends Axis {
    val other = Horizontal
  }
}