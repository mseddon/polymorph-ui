package polymorph.geom

sealed trait Side {
  def axis: Axis
  def opposite: Side
}
object Side {
  case object Left extends Side {
    def axis = Axis.Horizontal
    def opposite = Right
  }
  case object Right extends Side {
    def axis = Axis.Horizontal
    def opposite = Left
  }
  case object Top extends Side {
    def axis = Axis.Vertical
    def opposite = Bottom
  }
  case object Bottom extends Side {
    def axis = Axis.Vertical
    def opposite = Top
  }
}