package polymorph.geom

/**
 * Created by matt on 16/08/15.
 */
final case class Insets(left: Float, top: Float, right: Float, bottom: Float) {
  def width = left + right
  def height = top + bottom

  def size = Dimension(width, height)
}

object Insets {
  def apply(): Insets =
    Insets(0, 0, 0, 0)

  def apply(margin: Float): Insets =
    Insets(margin, margin, margin, margin)
}