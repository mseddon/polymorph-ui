package polymorph.geom

final case class Dimension(width: Float, height: Float) {
  def +(d: Dimension): Dimension =
    Dimension(width + d.width, height + d.height)

  def -(d: Dimension): Dimension =
    Dimension(width - d.width, height - d.height)

  def max(d: Dimension): Dimension =
    Dimension(width max d.width, height max d.height)

  def min(d: Dimension): Dimension =
    Dimension(width min d.width, height min d.height)

  def size(axis: Axis): Float = axis match {
    case Axis.Horizontal => width
    case Axis.Vertical => height
  }
}

object Dimension {
  def apply(): Dimension =
    Dimension(0, 0)
}