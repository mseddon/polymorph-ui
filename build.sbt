import scryetek.sbt.polymorph.Polymorph.autoImport._

name := "Scryetek Polymorph UI"

publish := {}
publishLocal := {}

lazy val polymorphUi = polymorphLibrary.in(file(".")).settings(
  name := "polymorph-ui",
  organization := "com.scryetek",
  version := "0.1-SNAPSHOT",
  libraryDependencies ++= Seq(
    polylib("com.scryetek" %%% "polymorph-core" % "0.1-SNAPSHOT"),
    polylib("com.scryetek" %%% "polymorph-gl" % "0.1-SNAPSHOT" % "provided")
  ),
  scalaVersion := "2.11.7",
  resolvers += "Nuonix Repo" at "https://repo-nuonix.rhcloud.com/artifactory/hijack-local",
  credentials += Credentials(Path.userHome / ".ivy2" / ".credentials"),
  publishTo := Some("Nuonix Repo" at "https://repo-nuonix.rhcloud.com/artifactory/hijack-local")
)

lazy val polymorphUiJS = polymorphUi.js
lazy val polymorphUiJVM = polymorphUi.jvm
lazy val polymorphUiIOS = polymorphUi.ios
lazy val polymorphUiAndroid = polymorphUi.android
